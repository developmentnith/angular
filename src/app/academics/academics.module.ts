import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitiesComponent } from './activities/activities.component';
import { FunctionariesComponent } from './functionaries/functionaries.component';
import { Admissions202021Component } from './admissions202021/admissions202021.component';
import { AdmissionsDeskComponent } from './admissions-desk/admissions-desk.component';
import { Registration202122Component } from './registration202122/registration202122.component';
import { ClassTimeTableComponent } from './class-time-table/class-time-table.component';



@NgModule({
  declarations: [
    ActivitiesComponent,
    FunctionariesComponent,
    Admissions202021Component,
    AdmissionsDeskComponent,
    Registration202122Component,
    ClassTimeTableComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AcademicsModule { }
