import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionsDeskComponent } from './admissions-desk.component';

describe('AdmissionsDeskComponent', () => {
  let component: AdmissionsDeskComponent;
  let fixture: ComponentFixture<AdmissionsDeskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmissionsDeskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionsDeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
