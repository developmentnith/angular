import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Admissions202021Component } from './admissions202021.component';

describe('Admissions202021Component', () => {
  let component: Admissions202021Component;
  let fixture: ComponentFixture<Admissions202021Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Admissions202021Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Admissions202021Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
