import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionariesComponent } from './functionaries.component';

describe('FunctionariesComponent', () => {
  let component: FunctionariesComponent;
  let fixture: ComponentFixture<FunctionariesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FunctionariesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
