import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Registration202122Component } from './registration202122.component';

describe('Registration202122Component', () => {
  let component: Registration202122Component;
  let fixture: ComponentFixture<Registration202122Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Registration202122Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Registration202122Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
