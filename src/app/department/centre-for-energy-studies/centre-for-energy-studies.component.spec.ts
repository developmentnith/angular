import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CentreForEnergyStudiesComponent } from './centre-for-energy-studies.component';

describe('CentreForEnergyStudiesComponent', () => {
  let component: CentreForEnergyStudiesComponent;
  let fixture: ComponentFixture<CentreForEnergyStudiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CentreForEnergyStudiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CentreForEnergyStudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
