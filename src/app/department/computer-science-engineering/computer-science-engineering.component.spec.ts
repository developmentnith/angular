import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputerScienceEngineeringComponent } from './computer-science-engineering.component';

describe('ComputerScienceEngineeringComponent', () => {
  let component: ComputerScienceEngineeringComponent;
  let fixture: ComponentFixture<ComputerScienceEngineeringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComputerScienceEngineeringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputerScienceEngineeringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
