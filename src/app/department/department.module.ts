import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChemistryComponent } from './chemistry/chemistry.component';
import { ComputerScienceEngineeringComponent } from './computer-science-engineering/computer-science-engineering.component';
import { CivilEngineeringComponent } from './civil-engineering/civil-engineering.component';
import { ChemicalEngineeringComponent } from './chemical-engineering/chemical-engineering.component';
import { ElectronicsCommunicationEngineeringComponent } from './electronics-communication-engineering/electronics-communication-engineering.component';
import { ElectricalEngineeringComponent } from './electrical-engineering/electrical-engineering.component';
import { MechanicalEngineeringComponent } from './mechanical-engineering/mechanical-engineering.component';
import { MaterialScienceEngineeringComponent } from './material-science-engineering/material-science-engineering.component';
import { MathematicsScientificComputingComponent } from './mathematics-scientific-computing/mathematics-scientific-computing.component';
import { PhysicsPhotonicsScienceComponent } from './physics-photonics-science/physics-photonics-science.component';
import { CentreForEnergyStudiesComponent } from './centre-for-energy-studies/centre-for-energy-studies.component';
import { ArchitectureComponent } from './architecture/architecture.component';
import { HumanitiesSocialSciencesComponent } from './humanities-social-sciences/humanities-social-sciences.component';
import { ManagementStudiesComponent } from './management-studies/management-studies.component';



@NgModule({
  declarations: [
    ChemistryComponent,
    ComputerScienceEngineeringComponent,
    CivilEngineeringComponent,
    ChemicalEngineeringComponent,
    ElectronicsCommunicationEngineeringComponent,
    ElectricalEngineeringComponent,
    MechanicalEngineeringComponent,
    MaterialScienceEngineeringComponent,
    MathematicsScientificComputingComponent,
    PhysicsPhotonicsScienceComponent,
    CentreForEnergyStudiesComponent,
    ArchitectureComponent,
    HumanitiesSocialSciencesComponent,
    ManagementStudiesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DepartmentModule { }
