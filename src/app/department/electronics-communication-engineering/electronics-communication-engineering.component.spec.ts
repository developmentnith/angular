import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicsCommunicationEngineeringComponent } from './electronics-communication-engineering.component';

describe('ElectronicsCommunicationEngineeringComponent', () => {
  let component: ElectronicsCommunicationEngineeringComponent;
  let fixture: ComponentFixture<ElectronicsCommunicationEngineeringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectronicsCommunicationEngineeringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicsCommunicationEngineeringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
