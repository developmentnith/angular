import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HumanitiesSocialSciencesComponent } from './humanities-social-sciences.component';

describe('HumanitiesSocialSciencesComponent', () => {
  let component: HumanitiesSocialSciencesComponent;
  let fixture: ComponentFixture<HumanitiesSocialSciencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HumanitiesSocialSciencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HumanitiesSocialSciencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
