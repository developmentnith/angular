import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementStudiesComponent } from './management-studies.component';

describe('ManagementStudiesComponent', () => {
  let component: ManagementStudiesComponent;
  let fixture: ComponentFixture<ManagementStudiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementStudiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementStudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
