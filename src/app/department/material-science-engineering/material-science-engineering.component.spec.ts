import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialScienceEngineeringComponent } from './material-science-engineering.component';

describe('MaterialScienceEngineeringComponent', () => {
  let component: MaterialScienceEngineeringComponent;
  let fixture: ComponentFixture<MaterialScienceEngineeringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaterialScienceEngineeringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialScienceEngineeringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
