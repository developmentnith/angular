import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MathematicsScientificComputingComponent } from './mathematics-scientific-computing.component';

describe('MathematicsScientificComputingComponent', () => {
  let component: MathematicsScientificComputingComponent;
  let fixture: ComponentFixture<MathematicsScientificComputingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MathematicsScientificComputingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MathematicsScientificComputingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
