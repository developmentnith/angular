import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mathematics-scientific-computing',
  templateUrl: './mathematics-scientific-computing.component.html',
  styleUrls: ['./mathematics-scientific-computing.component.css']
})
export class MathematicsScientificComputingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
