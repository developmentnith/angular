import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicsPhotonicsScienceComponent } from './physics-photonics-science.component';

describe('PhysicsPhotonicsScienceComponent', () => {
  let component: PhysicsPhotonicsScienceComponent;
  let fixture: ComponentFixture<PhysicsPhotonicsScienceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicsPhotonicsScienceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicsPhotonicsScienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
